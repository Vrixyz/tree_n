#![feature(try_trait)]

#[derive(Debug)]
struct Node<T> {
    pub value: T,
    pub children: Vec<Node<T>>,
}

#[derive(Debug)]
pub enum NodeTreeChildError {
    ChildNotFound,
}
impl From<std::option::NoneError> for NodeTreeChildError {
    fn from(_: std::option::NoneError) -> Self {
        NodeTreeChildError::ChildNotFound
    }
}

pub trait NodeTree<T>
where
    Self: Sized + std::fmt::Debug,
{
    fn child<'b, 'a: 'b>(&'a self, index: usize) -> Result<&'b Self, NodeTreeChildError>;
    fn children<'b, 'a: 'b>(&'a self) -> &'b Vec<Self>;
    fn value<'b, 'a: 'b>(&'a self) -> &'b T;
}
impl<T> NodeTree<T> for Node<T>
where
    T: std::fmt::Debug,
{
    fn child<'b, 'a: 'b>(&'a self, index: usize) -> Result<&'b Self, NodeTreeChildError> {
        let result = &self.children.get(index)?;
        Ok(result)
    }
    fn children<'b, 'a: 'b>(&'a self) -> &'b Vec<Self> {
        &self.children
    }
    fn value<'b, 'a: 'b>(&'a self) -> &'b T {
        &self.value
    }
}
pub struct TreeBuilder;
impl TreeBuilder {
    pub fn load() -> impl NodeTree<i32> {
        Node {
            value: 0,
            children: vec![
                Node {
                    value: 1,
                    children: vec![],
                },
                Node {
                    value: 2,
                    children: vec![],
                },
            ],
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn simple_tree() -> Result<(), NodeTreeChildError> {
        let tree = TreeBuilder::load();
        assert!(dbg!(tree.children().len()) > 0);
        dbg!(dbg!(tree.child(0)?).value());
        Ok(())
    }
}
